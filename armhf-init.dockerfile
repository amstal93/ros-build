ARG guest="arm32v7/debian:buster"
ARG host="debian:buster"

# arm64v8/debian:buster
# aarch64-linux-gnu
# arm64v8-buster-rpi4.cmake

FROM debian:buster as emu
RUN apt-get update -qq && DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends qemu-user-static

FROM $guest as guest
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 DEBIAN_FRONTEND=noninteractive
COPY --from=emu /usr/bin/qemu-* /usr/bin/
RUN apt-get update -qq && apt-get upgrade -qqy && apt-get install -qqy --no-install-recommends build-essential

FROM $host as host
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 DEBIAN_FRONTEND=noninteractive
ARG compiler="arm-linux-gnueabihf"
RUN apt-get update -qq && apt-get upgrade -qqy && \
    apt-get install -qqy "gcc-$compiler" "g++-$compiler"

LABEL maintainer "Vyacheslav Napadovsky" <napadovskiy@gmail.com>

RUN mkdir /rootgcc && mv /usr/bin/${compiler}-* /rootgcc
COPY --from=guest / /
RUN mv -f /rootgcc/* /usr/bin && rmdir /rootgcc

ARG toolchain=arm32v7-buster-rpi4.cmake
COPY $toolchain /opt/
ENV CMAKE_TOOLCHAIN_ARG="-DCMAKE_TOOLCHAIN_FILE=/opt/$toolchain"
