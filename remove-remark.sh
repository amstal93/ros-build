#!/bin/bash
set -e

PURGE=()
EXCLUDE=()
for p in "$@"; do
    if [[ "$p" != *'*'* ]]; then
        EXCLUDE+=("$p")
    fi
    if apt-cache show "$p" &>/dev/null; then
        PURGE+=("$p")
    fi
done

MARK_MANUAL=()
TEXT=()
START=0
while IFS='' read -r line; do
    echo "$line"
    case "$line" in
        "The following packages were automatically installed and are no longer required:")
            START=1
            continue
            ;;
        "The following packages will be REMOVED:")
            START=2
            continue
            ;;
    esac
    if [[ "$line" == "  "* ]]; then
        case "$START" in
            1)
                for pkg in $line; do
                    MARK_MANUAL+=( $pkg )
                done
                ;;
            2)
                for pkg in $line; do
                    EXCLUDE+=( ${pkg%\**} )
                done
                ;;
        esac
    else
        START=0
    fi
done < <(apt-get purge -y "${PURGE[@]}" || true)

PURGE=()
while IFS='' read -r line; do
    PURGE+=( "$line" )
done < <(dpkg --get-selections | grep \\Wdeinstall\$ | cut -f1)
if (( ${#PURGE[@]} > 0 )); then
    EXCLUDE+=("${PURGE[@]}")
    apt-get purge -y "${PURGE[@]}"
fi

if (( ${#MARK_MANUAL[@]} > 0 )); then
    apt-mark manual "${MARK_MANUAL[@]}"
    for pkg in "${MARK_MANUAL[@]}"; do echo "$pkg"; done >> "${DEPS_FILE}"
fi

if (( ${#EXCLUDE[@]} > 0 )); then
    for deb in "${EXCLUDE[@]}"; do echo "$deb"; done | sort -u >> "${DEPS_FILE}.exclude"

    DEPS="$(cat "$DEPS_FILE" | sort -u)"
    NEW_DEPS=()
    for deb in $DEPS; do
        FOUND=0
        for ex in "${EXCLUDE[@]}"; do
            if [[ "$deb" == "$ex" ]]; then
                FOUND=1
                break
            fi
        done
        if [[ "$FOUND" == 0 ]]; then
            NEW_DEPS+=( "$deb" )
        fi
    done

    for deb in "${NEW_DEPS[@]}"; do echo "$deb"; done > "$DEPS_FILE"
fi
