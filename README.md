# ROS-build
Automated ROS components builds for different platforms.

[Docker registry](https://gitlab.com/slavanap/ros-build/container_registry)

`docker pull registry.gitlab.com/slavanap/ros-build:bionic-tinker`

## Binary packages

### Desktop (amd64)

* [Ubuntu Bionic](https://gitlab.com/slavanap/ros-build/-/jobs/artifacts/master/download?job=bionic-amd64-deb)
* [Debian Stretch](https://gitlab.com/slavanap/ros-build/-/jobs/artifacts/master/download?job=stretch-amd64-deb)
* [Ubuntu Xenial](https://gitlab.com/slavanap/ros-build/-/blob/1f58dc0b687898535f6bd14f6afe461888517d8e/.gitlab-ci.yml#L67-76) (deprecated)

### Robot (armhf)

* [Raspberry Pi 4 x64 (Debian Buster)](https://gitlab.com/slavanap/ros-build/-/jobs/artifacts/master/download?job=buster-arm64v8-deb) (experimental)
* [ASUS Tinkerboard (Ubuntu Bionic)](https://gitlab.com/slavanap/ros-build/-/jobs/artifacts/master/download?job=bionic-tinker-deb)
* [Orange Pi board (Ubuntu Bionic)](https://gitlab.com/slavanap/ros-build/-/blob/9bf0cf525c61a1d9b09109a84731ea2145fc294f/.gitlab-ci.yml#L120-137) (temporarily dismissed)
* [Raspberry Pi 3 (Raspbian Stretch)](https://gitlab.com/slavanap/ros-build/blob/ec74fad42964e4ad0db8fb68ebca5bb2fcaadf98/.gitlab-ci.yml#L120-130) (deprecated)
