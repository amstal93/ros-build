set(CMAKE_CROSSCOMPILING TRUE)
set(CMAKE_LIBRARY_ARCHITECTURE arm-linux-gnueabihf)

set(CMAKE_C_COMPILER /usr/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER /usr/bin/arm-linux-gnueabihf-g++)

set(__MYFLAGS "-march=armv7ve")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${__MYFLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${__MYFLAGS}")

## TODO: try -mfpu=neon-vfpv4
## CFLAGS="-march=armv7ve -mfloat-abi=hard -mfpu=vfpv3-d16 -mtune=cortex-a17"
